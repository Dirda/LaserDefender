﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;

	// Use this for initialization
	void Start () {
		//initialization of all enemies
		foreach( Transform child in transform) {
			//create enemy prefab, cords, rotation
			GameObject enemy = Instantiate(enemyPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			enemy.transform.parent = transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

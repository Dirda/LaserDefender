﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 15.0f;
	public float padding = 1f;

	//Screen borders
	//No need no more - on start we create them	
	float xmin;// = -5;
	float xmax;// = 5;
	float ymin;
	float ymax;

	// Use this for initialization
	void Start () {

		//set starting position
		transform.position = new Vector3(transform.position.x, -4, transform.position.z);

		float distanceBetweenCameraAndObject = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceBetweenCameraAndObject));
		Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distanceBetweenCameraAndObject));
		
		Vector3 topMost = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distanceBetweenCameraAndObject));
		Vector3 bottomMost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceBetweenCameraAndObject));

		xmin = leftMost.x + padding;
		xmax = rightMost.x - padding;
		ymin = bottomMost.y + padding;
		ymax = topMost.y - padding;
	}
	
	// Update is called once per frame
	void Update () {
		
		//movement by frame Time.deltaTime
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey("a")) {
			transform.position += Vector3.left * speed * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey("d")) {
			//transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
			transform.position += Vector3.right * speed * Time.deltaTime;
		} else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey("w")) {
			//transform.position += new Vector3(0, speed * Time.deltaTime, 0);
			transform.position += Vector3.up * speed * Time.deltaTime;	
		} else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey("s")) {
			//transform.position += new Vector3(0, -speed * Time.deltaTime, 0);	
			transform.position += Vector3.down * speed * Time.deltaTime;	
		}

		//restriction for a player to do not cross to out of bound.
		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		float newY = Mathf.Clamp(transform.position.y, ymin, ymax);
		transform.position = new Vector3(newX, newY, transform.position.z);
	}
}
